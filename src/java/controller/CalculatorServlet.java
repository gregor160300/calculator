/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author gregor
 */
@WebServlet(urlPatterns = {"/calculator"})
public class CalculatorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        String function = request.getParameter("function");
        String number = "";
        String c = "";
        String savedNumber = "";
        String result = "";
        // session.setAttribute("savedNumber", number + savedNumber);

        result = request.getParameter("result");

        if (function == null) {
            number = request.getParameter("number");
            savedNumber = request.getParameter("savedNumber");
            c = request.getParameter("c");
        } else {
            // don't destroy the previously saved session attribute in the next if
            savedNumber = null;
            // don't destroy the session
            c = null;
        }

        // putting the numbers together and saving in a session attribute
        if (savedNumber != null) {
            session.setAttribute("savedNumber", savedNumber + number);
        }

        if (function != null) {
            String tempNumber = (String) session.getAttribute("savedNumber");
            if (!tempNumber.equals("")) {
                float number1 = Float.parseFloat(tempNumber);
                session.setAttribute("number1", number1);
                // empty the savedNumber because there is now a session attribute number1
                session.setAttribute("savedNumber", "");
            }

            if (function.equals("+")
                    || function.equals("-")
                    || function.equals("*")
                    || function.equals("/")) {
                session.setAttribute("function", function);
            }
        }

        try {
            if (session != null) {
                if ("c".equals(c)) {
                    session.invalidate();
                    request.getRequestDispatcher("/index.jsp").forward(request, response);
                }
            }
        } catch (IllegalStateException ex) {
            // do nothing
        }

        if (result != null && result.equals("=")) {
            float number1 = (float) session.getAttribute("number1");
            float number2 = Float.parseFloat(savedNumber);
            session.setAttribute("savedNumber", "");
            session.setAttribute("number2", number2);
            // temporary code with a plus - must be replaced with a switch and the calculator Object
            String calculationFunction = (String) session.getAttribute("function");
            switch (calculationFunction) {
                case "/":
                    session.setAttribute("calculated", number1 / number2);
                    break;
                case "*":
                    session.setAttribute("calculated", number1 * number2);
                    break;
                case "+":
                    session.setAttribute("calculated", number1 + number2);
                    break;
                case "-":
                    session.setAttribute("calculated", number1 - number2);
                    break;
                default:
                    break;
            }
        }

        String url = "/index.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
