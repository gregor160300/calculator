<%-- 
    Document   : index
    Created on : Feb 28, 2017, 10:34:20 AM
    Author     : gregor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Calculator</title>
    </head>
    <body>
        <h1>Calculator</h1>
        <form action="calculator">
            <h3>Invoer: ${savedNumber}</h3>
            <h3>${number1} ${function} ${number2} = ${calculated}</h3>
            <input type="submit" name="number" value="1" />
            <input type="submit" name="number" value="2" />
            <input type="submit" name="number" value="3" />
            <input type="submit" name="number" value="4" />
            <input type="submit" name="number" value="5" />
            <input type="submit" name="number" value="6" />
            <input type="submit" name="number" value="7" />
            <input type="submit" name="number" value="8" />
            <input type="submit" name="number" value="9" />
            <input type="submit" name="number" value="0" />
            <input type="submit" name="function" value="+" />
            <input type="submit" name="function" value="-" />
            <input type="submit" name="function" value="/" />
            <input type="submit" name="function" value="*" />
            <input type="submit" name="c" value="c" />
            <input type="submit" name="result" value="=" />
            <input type="hidden" name="savedNumber" value="${savedNumber}" />
        </form>
    </body>
</html>
